from conan import ConanFile

class DataFitterRecipe(ConanFile):
    name = "datafitter"
    executable = "ds_DataFitter"
    version = "3.4.5"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois, Ludmila Klenov, Julien Berthault"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/calculation/datafitter.git"
    description = "DataFitter device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("datafitterlib/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("gsl/1.11@soleil/stable")
        self.requires("boostlibraries/1.65.1a@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
