
static const char *RcsId = "$Header:  $";

static const char *TagName = "$Name:  $";

static const char *FileName= "$Source:  $"; 

static const char *HttpServer= "http://controle/DeviceServer/doc/";

static const char *RCSfile = "$RCSfile:  $"; 

//+=============================================================================
//
// file :        DataFitterTestClass.cpp
//
// description : C++ source for the DataFitterTestClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the DataFitterTest once per process.
//
// project :     TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
//
// $Log:  $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#pragma warning( push )
#pragma warning( disable : 4312 )
#pragma warning( disable : 4311 )
#pragma warning( disable : 4267 )
#include <tango.h>
#include <DataFitterTestClass.h>
#pragma warning( pop )

#include <DataFitterTest.h>

namespace DataFitterTest_ns
{

//+----------------------------------------------------------------------------
//
// method : 		StartClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StartClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StartClass::execute(): arrived" << endl;

	Tango::DevString	argin;
	extract(in_any, argin);

	((static_cast<DataFitterTest *>(device))->start(argin));
	return new CORBA::Any();
}


//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
DataFitterTestClass *DataFitterTestClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::DataFitterTestClass(string &s)
// 
// description : 	constructor for the DataFitterTestClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
DataFitterTestClass::DataFitterTestClass(string &s):DeviceClass(s)
{

	cout2 << "Entering DataFitterTestClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving DataFitterTestClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::~DataFitterTestClass()
// 
// description : 	destructor for the DataFitterTestClass
//
//-----------------------------------------------------------------------------
DataFitterTestClass::~DataFitterTestClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
DataFitterTestClass *DataFitterTestClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new DataFitterTestClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

DataFitterTestClass *DataFitterTestClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void DataFitterTestClass::command_factory()
{
	command_list.push_back(new StartClass("Start",
		Tango::DEV_STRING, Tango::DEV_VOID,
		"Type of the function to generate (Gaussian, Lorentzian)",
		"",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum DataFitterTestClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum DataFitterTestClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum DataFitterTestClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void DataFitterTestClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new DataFitterTest(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: DataFitterTestClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void DataFitterTestClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : generatedDataX
	generatedDataXAttrib	*generated_data_x = new generatedDataXAttrib();
	Tango::UserDefaultAttrProp	generated_data_x_prop;
	generated_data_x_prop.set_label("Generated Data X");
	generated_data_x_prop.set_description("Generated Data according X axis.");
	generated_data_x->set_default_properties(generated_data_x_prop);
	att_list.push_back(generated_data_x);

	//	Attribute : generatedDataY
	generatedDataYAttrib	*generated_data_y = new generatedDataYAttrib();
	Tango::UserDefaultAttrProp	generated_data_y_prop;
	generated_data_y_prop.set_label("Generated Data Y");
	generated_data_y_prop.set_description("Generated Data according Y axis.");
	generated_data_y->set_default_properties(generated_data_y_prop);
	att_list.push_back(generated_data_y);

	//	Attribute : generatedDataSigma
	generatedDataSigmaAttrib	*generated_data_sigma = new generatedDataSigmaAttrib();
	Tango::UserDefaultAttrProp	generated_data_sigma_prop;
	generated_data_sigma_prop.set_label("Generated Data Sigma");
	generated_data_sigma_prop.set_description("Generated Data for Sigma.");
	generated_data_sigma->set_default_properties(generated_data_sigma_prop);
	att_list.push_back(generated_data_sigma);

	//	Attribute : nbPointsToGenerate
	nbPointsToGenerateAttrib	*nb_points_to_generate = new nbPointsToGenerateAttrib();
	Tango::UserDefaultAttrProp	nb_points_to_generate_prop;
	nb_points_to_generate_prop.set_label("Nb Points To Generate");
	nb_points_to_generate_prop.set_format("%d");
	nb_points_to_generate_prop.set_max_value("100000");
	nb_points_to_generate_prop.set_min_value("1");
	nb_points_to_generate_prop.set_description("Number of points to generate for the choosen function.");
	nb_points_to_generate->set_default_properties(nb_points_to_generate_prop);
	nb_points_to_generate->set_memorized();
	nb_points_to_generate->set_memorized_init(false);
	att_list.push_back(nb_points_to_generate);

	//	Attribute : resolutionX
	resolutionXAttrib	*resolution_x = new resolutionXAttrib();
	Tango::UserDefaultAttrProp	resolution_x_prop;
	resolution_x_prop.set_label("Resolution X");
	resolution_x_prop.set_format("%6.5f");
	resolution_x_prop.set_max_value("100000");
	resolution_x_prop.set_min_value("0.00001");
	resolution_x_prop.set_description("Resolution according the X axis to generate \nthe choosen function.");
	resolution_x->set_default_properties(resolution_x_prop);
	resolution_x->set_memorized();
	resolution_x->set_memorized_init(false);
	att_list.push_back(resolution_x);

	//	Attribute : startingX
	startingXAttrib	*starting_x = new startingXAttrib();
	Tango::UserDefaultAttrProp	starting_x_prop;
	starting_x_prop.set_label("Starting X");
	starting_x_prop.set_format("%6.5f");
	starting_x_prop.set_description("Starting value for X to generate the generatedDataX \nspectrum values.");
	starting_x->set_default_properties(starting_x_prop);
	starting_x->set_memorized();
	starting_x->set_memorized_init(false);
	att_list.push_back(starting_x);

	//	Attribute : noiseFactor
	noiseFactorAttrib	*noise_factor = new noiseFactorAttrib();
	Tango::UserDefaultAttrProp	noise_factor_prop;
	noise_factor_prop.set_label("Noise Factor");
	noise_factor_prop.set_format("%6.5f");
	noise_factor_prop.set_description("Noise factor to add at each calculated value.");
	noise_factor->set_default_properties(noise_factor_prop);
	noise_factor->set_memorized();
	noise_factor->set_memorized_init(false);
	att_list.push_back(noise_factor);

	//	Attribute : functionParameterPos
	functionParameterPosAttrib	*function_parameter_pos = new functionParameterPosAttrib();
	Tango::UserDefaultAttrProp	function_parameter_pos_prop;
	function_parameter_pos_prop.set_label("Position");
	function_parameter_pos_prop.set_format("%6.5f");
	function_parameter_pos_prop.set_description("This is the position function parameter for the implemented\n functions.");
	function_parameter_pos->set_default_properties(function_parameter_pos_prop);
	function_parameter_pos->set_memorized();
	function_parameter_pos->set_memorized_init(false);
	att_list.push_back(function_parameter_pos);

	//	Attribute : functionParameterWidth
	functionParameterWidthAttrib	*function_parameter_width = new functionParameterWidthAttrib();
	Tango::UserDefaultAttrProp	function_parameter_width_prop;
	function_parameter_width_prop.set_label("Width");
	function_parameter_width_prop.set_format("%6.5f");
	function_parameter_width_prop.set_description("This is the width function parameter for the implemented \nfunctions.");
	function_parameter_width->set_default_properties(function_parameter_width_prop);
	function_parameter_width->set_memorized();
	function_parameter_width->set_memorized_init(false);
	att_list.push_back(function_parameter_width);

	//	Attribute : functionParameterHeight
	functionParameterHeightAttrib	*function_parameter_height = new functionParameterHeightAttrib();
	Tango::UserDefaultAttrProp	function_parameter_height_prop;
	function_parameter_height_prop.set_label("Height");
	function_parameter_height_prop.set_format("%6.5f");
	function_parameter_height_prop.set_description("This is the height function parameter for the implemented \nfunctions.");
	function_parameter_height->set_default_properties(function_parameter_height_prop);
	function_parameter_height->set_memorized();
	function_parameter_height->set_memorized_init(false);
	att_list.push_back(function_parameter_height);

	//	Attribute : functionParameterBackground
	functionParameterBackgroundAttrib	*function_parameter_background = new functionParameterBackgroundAttrib();
	Tango::UserDefaultAttrProp	function_parameter_background_prop;
	function_parameter_background_prop.set_label("Background");
	function_parameter_background_prop.set_format("%6.5f");
	function_parameter_background_prop.set_max_value("100000");
	function_parameter_background_prop.set_min_value("-100000");
	function_parameter_background_prop.set_description("Value of the background for the implemented functions.");
	function_parameter_background->set_default_properties(function_parameter_background_prop);
	function_parameter_background->set_memorized();
	function_parameter_background->set_memorized_init(false);
	att_list.push_back(function_parameter_background);

	//	Attribute : sigma
	sigmaAttrib	*sigma = new sigmaAttrib();
	Tango::UserDefaultAttrProp	sigma_prop;
	sigma_prop.set_label("Sigma");
	sigma_prop.set_format("%6.5f");
	sigma_prop.set_description("This is the sigma function error parameter for the gaussian and\nlorentzian functions.\nThis value represent the estimation erro for each points.");
	sigma->set_default_properties(sigma_prop);
	att_list.push_back(sigma);

	//	Attribute : functionType
	functionTypeAttrib	*function_type = new functionTypeAttrib();
	Tango::UserDefaultAttrProp	function_type_prop;
	function_type_prop.set_label("Function Type");
	function_type_prop.set_description("Describe the type of function to generate \n(Gaussian, Lorentzian or Sigmoid with ou without \nbackground)\n- gaussian(b)\n- lorentzian(b)\n- sigmoid(b)");
	function_type->set_default_properties(function_type_prop);
	function_type->set_memorized();
	function_type->set_memorized_init(false);
	att_list.push_back(function_type);

	//	Attribute : filePath
	filePathAttrib	*file_path = new filePathAttrib();
	file_path->set_memorized();
	file_path->set_memorized_init(true);
	att_list.push_back(file_path);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void DataFitterTestClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	DataFitterTestClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void DataFitterTestClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "FilePath";
	prop_desc = "";
	prop_def  = "";
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTestClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void DataFitterTestClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Functions Generation");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("Device to test the fitting device by generating functions with noise (Gaussian, Lorentzian");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs location
	string	rcsId(RcsId);
	string	filename(classname);
	start = rcsId.find("/");
	if (start!=string::npos)
	{
		filename += "Class.cpp";
		end   = rcsId.find(filename);
		if (end>start)
		{
			string	strloc = rcsId.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}

	//	Get CVS tag revision
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_3Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
