static const char *RcsId = "$Header:  $";
//+=============================================================================
//
// file :         DataFitterTestStateMachine.cpp
//
// description :  C++ source for the DataFitterTest and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
//
// $Log:  $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#pragma warning( push )
#pragma warning( disable : 4312 )
#pragma warning( disable : 4311 )
#pragma warning( disable : 4267 )
#include <tango.h>
#include <DataFitterTestClass.h>
#pragma warning( pop )

#include <DataFitterTest.h>


/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace DataFitterTest_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_generatedDataX_allowed
// 
// description : 	Read/Write allowed for generatedDataX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_generatedDataX_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_generatedDataY_allowed
// 
// description : 	Read/Write allowed for generatedDataY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_generatedDataY_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_generatedDataSigma_allowed
// 
// description : 	Read/Write allowed for generatedDataSigma attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_generatedDataSigma_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_nbPointsToGenerate_allowed
// 
// description : 	Read/Write allowed for nbPointsToGenerate attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_nbPointsToGenerate_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_resolutionX_allowed
// 
// description : 	Read/Write allowed for resolutionX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_resolutionX_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_startingX_allowed
// 
// description : 	Read/Write allowed for startingX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_startingX_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_noiseFactor_allowed
// 
// description : 	Read/Write allowed for noiseFactor attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_noiseFactor_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_functionParameterPos_allowed
// 
// description : 	Read/Write allowed for functionParameterPos attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_functionParameterPos_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_functionParameterWidth_allowed
// 
// description : 	Read/Write allowed for functionParameterWidth attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_functionParameterWidth_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_functionParameterHeight_allowed
// 
// description : 	Read/Write allowed for functionParameterHeight attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_functionParameterHeight_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_functionParameterBackground_allowed
// 
// description : 	Read/Write allowed for functionParameterBackground attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_functionParameterBackground_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_sigma_allowed
// 
// description : 	Read/Write allowed for sigma attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_sigma_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_functionType_allowed
// 
// description : 	Read/Write allowed for functionType attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_functionType_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_filePath_allowed
// 
// description : 	Read/Write allowed for filePath attribute.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_filePath_allowed(Tango::AttReqType type)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		DataFitterTest::is_Start_allowed
// 
// description : 	Execution allowed for Start command.
//
//-----------------------------------------------------------------------------
bool DataFitterTest::is_Start_allowed(const CORBA::Any &any)
{
	if (get_state() == Tango::FAULT)
	{
		//	End of Generated Code

		//	Re-Start of Generated Code
		return false;
	}
	return true;
}

}	// namespace DataFitterTest
