#ifdef WIN32
#  pragma warning ( disable : 4244 )
#endif

#include <numeric>
#include <cmath>
#include <yat/threading/Task.h>
#include <tango.h>
#include "DataFitterTask.h"
// #include "Exception.h"
//debut TANGODEVIC-235
#include <iostream>
//fin TANGODEVIC-235

namespace DataFitter_ns
{
    enum
    {
        MSGID_CONFIGURE_FIT = yat::FIRST_USER_MSG,
        MSGID_CONFIGURE_SOURCE,
        MSGID_CONFIGURE_GENERATION,
        MSGID_CONFIGURE_ROI,
        MSGID_SET_MODE,
        MSGID_DOFIT
    };

    //------------------------------------------------------------------//
    //
    //	Utilities
    //	throw_devfailed
    //
    //------------------------------------------------------------------//

    void throw_devfailed(const char* reason, const char* desc, const char* origin)
    {
        Tango::DevFailed df;
        df.errors.length(1);
        df.errors[0].reason = CORBA::string_dup(reason);
        df.errors[0].desc = CORBA::string_dup(desc);
        df.errors[0].origin = CORBA::string_dup(origin);
        df.errors[0].severity = Tango::ERR;
        throw df;
    }
    //------------------------------------------------------------------//
    //
    //	Utilities
    //	throw_devfailed
    //
    //------------------------------------------------------------------//

    //- yat::Exception -> Tango::DevFailed
    void throw_devfailed(yat::Exception& yat_ex)
    {
        Tango::DevFailed df;
        const yat::Exception::ErrorList& yat_errors = yat_ex.errors;
        df.errors.length(yat_errors.size());
        for (size_t i = 0; i < yat_errors.size(); i++)
        {
            df.errors[i].reason = CORBA::string_dup(yat_errors[i].reason.c_str());
            df.errors[i].desc = CORBA::string_dup(yat_errors[i].desc.c_str());
            df.errors[i].origin = CORBA::string_dup(yat_errors[i].origin.c_str());
            df.errors[i].severity = static_cast<Tango::ErrSeverity>(yat_errors[i].severity);
        }
        throw df;
    }
    //------------------------------------------------------------------//
    //
    //	Utilities
    //	datafitter_gsl_error_handler
    //
    //------------------------------------------------------------------//

    void datafitter_gsl_error_handler(const char * reason, const char * file,
        int line, int gsl_errno)
    {
        std::ostringstream reason_s;
        reason_s << "GSL error " << gsl_errno << " caught : " << reason << std::ends;

        std::ostringstream origin_s;
        origin_s << file << "[" << line << "]" << std::ends;

        throw_devfailed("GSL_ERROR",
            reason_s.str().c_str(),
            origin_s.str().c_str());
    }
    //------------------------------------------------------------------//
    //
    //	Utilities
    //	extract_data_as_double
    //
    //------------------------------------------------------------------//


    void extract_data_as_double(Tango::DeviceAttribute& da, Tango::AttributeInfo& info, std::vector<double>& vec)
    {

        vec.resize(da.dim_x);

        if (da.dim_x > 0)
        {
            switch (info.data_type)
            {
            case Tango::DEV_BOOLEAN:
                std::copy(da.BooleanSeq->get_buffer(), da.BooleanSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_UCHAR:
                std::copy(da.UCharSeq->get_buffer(), da.UCharSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_SHORT:
                std::copy(da.ShortSeq->get_buffer(), da.ShortSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_USHORT:
                std::copy(da.UShortSeq->get_buffer(), da.UShortSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_LONG:
                std::copy(da.LongSeq->get_buffer(), da.LongSeq->get_buffer() + da.dim_x, vec.begin());
                break;
#if (TANGO_VERSION_MAJOR >= 9)
#pragma message "TANGO_VERSION_MAJOR >= 9"
            case Tango::DEV_ULONG:
                std::copy( da.ULongSeq->get_buffer(), da.ULongSeq->get_buffer() + da.dim_x, vec.begin() );
                break;
            case Tango::DEV_LONG64:
                std::copy( da.Long64Seq->get_buffer(), da.Long64Seq->get_buffer() + da.dim_x, vec.begin() );
                break;
            case Tango::DEV_ULONG64:
                std::copy( da.ULong64Seq->get_buffer(), da.ULong64Seq->get_buffer() + da.dim_x, vec.begin() );
                break;
#elif (TANGO_VERSION_MAJOR >= 8)
#pragma message "TANGO_VERSION_MAJOR >= 8"
            case Tango::DEV_ULONG:
                std::copy( da.get_ULong_data()->get_buffer(), da.get_ULong_data()->get_buffer() + da.dim_x, vec.begin() );
                break;
            case Tango::DEV_LONG64:
                std::copy( da.get_Long64_data()->get_buffer(), da.get_Long64_data()->get_buffer() + da.dim_x, vec.begin() );
                break;
            case Tango::DEV_ULONG64:
                std::copy( da.get_ULong64_data()->get_buffer(), da.get_ULong64_data()->get_buffer() + da.dim_x, vec.begin() );
                break;
#else
#pragma message "TANGO_VERSION_MAJOR < 8"
            case Tango::DEV_ULONG:
                std::copy(da.ext->ULongSeq->get_buffer(), da.ext->ULongSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_LONG64:
                std::copy(da.ext->Long64Seq->get_buffer(), da.ext->Long64Seq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_ULONG64:
                std::copy(da.ext->ULong64Seq->get_buffer(), da.ext->ULong64Seq->get_buffer() + da.dim_x, vec.begin());
                break;
#endif
            case Tango::DEV_FLOAT:
                std::copy(da.FloatSeq->get_buffer(), da.FloatSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            case Tango::DEV_DOUBLE:
                std::copy(da.DoubleSeq->get_buffer(), da.DoubleSeq->get_buffer() + da.dim_x, vec.begin());
                break;
            default:
                break;
            }
        }
    }
    //------------------------------------------------------------------//
    //
    //	CTOR
    //	DataFitterTask::DataFitterTask
    //
    //------------------------------------------------------------------//


    DataFitterTask::DataFitterTask(Tango::DeviceImpl* dev)
        : yat::Task(),
        Tango::LogAdapter(dev),
        fit_ready_(false),
        manual_mode_(true),
        source_ready_(false),
        state_(Tango::INIT),
        status_("Initializing...")
    {
        this->enable_periodic_msg(false);
        this->enable_timeout_msg(false);
        this->go();
    }
    //------------------------------------------------------------------//
    //
    //	DTOR
    //	DataFitterTask::DataFitterTask
    //
    //------------------------------------------------------------------//

    DataFitterTask::~DataFitterTask()
    {
    }

    //------------------------------------------------------------------//
    //
    //	DataFitterTask::  internal methods to call YAT messages
    //
    //------------------------------------------------------------------//

    void DataFitterTask::configure_fit(DataFitterConfig::FitParams& params)
    {
        try
        {
            yat::Message* msg = yat::Message::allocate(MSGID_CONFIGURE_FIT, DEFAULT_MSG_PRIORITY, true);
            msg->attach_data(params);
            this->wait_msg_handled(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }

    void DataFitterTask::configure_source(DataFitterConfig::DataSource& data_source)
    {
        try
        {
            yat::Message* msg = yat::Message::allocate(MSGID_CONFIGURE_SOURCE, DEFAULT_MSG_PRIORITY, true);
            msg->attach_data(data_source);
            this->wait_msg_handled(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }

    void DataFitterTask::configure_generation(DataFitterConfig::GenerationParams& fitted_data)
    {
        try
        {
            yat::Message* msg = yat::Message::allocate(MSGID_CONFIGURE_GENERATION, DEFAULT_MSG_PRIORITY, true);
            msg->attach_data(fitted_data);
            this->wait_msg_handled(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }

    void DataFitterTask::configureROI(DataFitterConfig::ROI_Params& oROI_Params)
    {
        try
        {
            yat::Message* msg = yat::Message::allocate(MSGID_CONFIGURE_ROI, DEFAULT_MSG_PRIORITY, true);
            msg->attach_data(oROI_Params);
            this->wait_msg_handled(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }

    void DataFitterTask::set_manual_mode(bool manual)
    {
        try
        {
            yat::Message* msg = yat::Message::allocate(MSGID_SET_MODE, DEFAULT_MSG_PRIORITY, true);
            msg->attach_data(manual ? static_cast<char>(1) : static_cast<char>(0));
            this->wait_msg_handled(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }

    void DataFitterTask::start()
    {
        try
        {
            set_state_status(Tango::RUNNING, "starting fit");
            yat::Message* msg = yat::Message::allocate(MSGID_DOFIT, DEFAULT_MSG_PRIORITY, true);
            this->post(msg);
        }
        catch (yat::Exception& ex)
        {
            throw_devfailed(ex);
        }
    }
    //------------------------------------------------------------------//
    //
    //	DataFitterTask:: YAT handle_message
    //
    //------------------------------------------------------------------//

    void DataFitterTask::handle_message(yat::Message& msg)
    {
        try
        {
            switch (msg.type())
            {
            case yat::TASK_INIT:
            {
                gsl_error_handler_t* err_handler = &datafitter_gsl_error_handler;
                gsl_set_error_handler(err_handler);

                state_ = Tango::STANDBY;
                status_ = "Waiting for fit request";
            }
            break;
            case yat::TASK_EXIT:
            {
            }
            break;
            case yat::TASK_TIMEOUT:
            {
                this->post(yat::Message::allocate(MSGID_DOFIT));
            }
            break;
            case MSGID_CONFIGURE_FIT:
            {
                config_.fit_params = msg.get_data<DataFitterConfig::FitParams>();
                this->configure_fit_i();
                //debut TANGODEVIC-1339
                //if configuration of the fit is OK then device status is STANDBY
                if (fit_ready_)
                    this->set_state_status(Tango::STANDBY, "Waiting for fit request");
                //fin TANGODEVIC-1339
            }
            break;
            case MSGID_CONFIGURE_SOURCE:
            {
                config_.data_source = msg.get_data<DataFitterConfig::DataSource>();
                this->configure_source_i();
            }
            break;
            case MSGID_CONFIGURE_GENERATION:
            {
                config_.fitted_data = msg.get_data<DataFitterConfig::GenerationParams>();
            }
            break;
            case MSGID_SET_MODE:
            {
                manual_mode_ = msg.get_data<char>() ? true : false;
                this->enable_timeout_msg(!manual_mode_);
                this->set_timeout_msg_period(300);

                if (manual_mode_)
                {
                    state_ = Tango::STANDBY;
                    status_ = "Waiting for fit request";
                }

            }
            break;
            case MSGID_CONFIGURE_ROI:
            {
                config_.m_oROI_params = msg.get_data<DataFitterConfig::ROI_Params>();
            }
            break;
            case MSGID_DOFIT:
            {

                // if something was wrong at configure time
                if (!fit_ready_ || !fit_function_)
                    this->configure_fit_i();

                // MANTIS 22337
                // We need to reconfigure source at each fit as data may change of type from one fit to another (like data provided by the ScanServer)
                this->configure_source_i();


                if (!fit_ready_ || !fit_function_)
                {
                    this->set_state_status(Tango::FAULT, "Fit function not properly initialized");
                }
                else if (!source_ready_)
                {
                    this->set_state_status(Tango::FAULT, "Source attributes not correctly set");
                }
                else
                {

                    FitStatus fit_status = this->dofit();

                    if (fit_status.state == Tango::RUNNING) // everything is fine
                    {
                        if (manual_mode_)
                            this->set_state_status(Tango::STANDBY, "Waiting for fit request");
                        else
                            this->set_state_status(Tango::RUNNING, "Automatic Fit in progress");
                    }
                    else
                    {
                        this->set_state_status(fit_status.state, fit_status.status);
                    }

                }
            }
            break;
            }
        }
        catch (yat::Exception& ex)
        {
            ERROR_STREAM << ex.errors[0].desc << " in " << __FILE__ << " (line " << __LINE__ << ")" << ENDLOG;
            this->set_state_status(Tango::FAULT, ex.errors[0].desc);
            //debut TANGODEVIC-1263
            this->reset_data();
            //fin TANGODEVIC-1263

        }
        catch (Tango::DevFailed& ex)
        {
            ERROR_STREAM << ex << " in " << __FILE__ << " (line " << __LINE__ << ")" << ENDLOG;
            this->set_state_status(Tango::FAULT, ex.errors[0].desc._ptr);
            //debut TANGODEVIC-1263
            this->reset_data();
            //fin TANGODEVIC-1263

        }
    }

    void DataFitterTask::configure_fit_i()
    {
        //DEBUG_STREAM << "in DataFitterTask::configure_fit_i()" << ENDLOG;
        fit_ready_ = false;
        FittingFunctionFactory factory;
        FittingFunction* fun = factory.newFitter(config_.fit_params.fitting_function_type.str);
        if (!fun)
            //debut TANGODEVIC-1263
            throw_devfailed("BAD_PARAMETER", "Invalid fitting function type. See attribute 'knownFittersList' for the list of the valid functions.", "DataFitter::configure_fit_i");
        //fin TANGODEVIC-1263
        fit_function_.reset(fun);
        fit_function_equation_ = fit_function_->getEquation().c_str();
        fit_ready_ = true;
    }

    void DataFitterTask::configure_source_i()
    {
        DEBUG_STREAM << "in DataFitterTask::configure_source_i()" << ENDLOG;

        DEBUG_STREAM << "attr name x: " << config_.data_source.attr_name_x.str << ENDLOG;
        DEBUG_STREAM << "attr name y: " << config_.data_source.attr_name_y.str << ENDLOG;
        DEBUG_STREAM << "attr name s: " << config_.data_source.attr_name_s.str << ENDLOG;
        DEBUG_STREAM << "use s ?: " << (config_.data_source.use_sigma ? "true" : "false") << ENDLOG;

        try {

            source_ready_ = false;

            source_x_.reset(new Tango::AttributeProxy(config_.data_source.attr_name_x.str));
            // MANTIS 24726
            if (source_x_)
                source_x_info_ = source_x_->get_config();
            else
                return;

            source_y_.reset(new Tango::AttributeProxy(config_.data_source.attr_name_y.str));
            if (source_y_)
                source_y_info_ = source_y_->get_config();
            else
                return;

            if (config_.data_source.use_sigma)
            {
                source_sigma_.reset(new Tango::AttributeProxy(config_.data_source.attr_name_s.str));
                source_sigma_info_ = source_sigma_->get_config();
            }

            source_ready_ = true;

        }
        catch (Tango::DevFailed& df) {

            Tango::Except::re_throw_exception(df, "CONFIGURATION_FAILED", "unable to configure sources", "DataFitterTask::configure_source_i");

        }


    }

    //debut TANGODEVIC-1263
    //------------------------------------------------------------------//
    //
    //	DataFitterTask::reset_data()
    //
    //------------------------------------------------------------------//
    void DataFitterTask::reset_data()
    {//destroy structure data. Number attributes will be setted to NAN.
        yat::MutexLock guard(this->mutex_);
        data_.reset();
    }

    //fin TANGODEVIC-1263

    //------------------------------------------------------------------//
    //
    //	DataFitterTask::checkROI_Coherency(...)
    //
    // Find the indexes corresponding to the range of the ROI in the Data.
    // If possible, the values are included in the range, as expected by the user:
    // We consider the range [dMinRange; dMaxRange] instead of [dMinRange; dMaxRange[
    // , ]dMinRange; dMaxRange], or ]dMinRange; dMaxRange[.
    //
    // Note: The main searching loop is the easiest solution to set up to deal with small vectors,
    //       but other algorithms could be adapted if a speed enhancement is required (see "binary search").
    //
    //------------------------------------------------------------------//
    void DataFitterTask::checkROI_Coherency(std::vector<double> &oData
        , double dMinRange
        , double dMaxRange
        , size_t& oROI_FirstIdx
        , size_t& oROI_LastIdx)
    {
        oROI_FirstIdx = 0;
        oROI_LastIdx = 0;

        // The values must be ordered
        if (dMinRange > dMaxRange)
            throw_devfailed("BAD_PARAMETER", "Experimental DataX Min and Max Range "
            "are incoherents (Min must be inferior to Max).", "DataFitterTask::dofit");

        if (dMinRange == dMaxRange)
        {
            // The ROI corresponds to the entire data vector (the ROI values doesn't matters)
            oROI_FirstIdx = 0;
            oROI_LastIdx = oData.size() - 1;
            return;
        }

        // Check ROI and range
        bool bMinIsBelowRange = dMinRange < oData.front();
        bool bMinIsAboveRange = dMinRange > oData.back();
        bool bMaxIsBelowRange = dMaxRange < oData.front();
        bool bMaxIsAboveRange = dMaxRange > oData.back();

        if ((bMinIsBelowRange && bMaxIsBelowRange)
            || (bMinIsAboveRange && bMaxIsAboveRange))
            throw_devfailed("BAD_PARAMETER", "Experimental DataX Min and Max Range "
            "are both outside the Data.", "DataFitterTask::dofit");

        if (bMinIsBelowRange && bMaxIsAboveRange)
        {
            // The ROI corresponds to the entire data vector
            oROI_FirstIdx = 0;
            oROI_LastIdx = oData.size() - 1;
            return;
        }

        // Searching the first and last indexes
        double dCurrentValue = 0.0;
        for (size_t iCurrentIdx = 0; iCurrentIdx < oData.size(); iCurrentIdx++)
        {
            dCurrentValue = oData[iCurrentIdx];
            if (dCurrentValue <= dMinRange) oROI_FirstIdx = iCurrentIdx;
            if ((dCurrentValue >= dMaxRange) || (iCurrentIdx == (oData.size() - 1) ) )
            {
                oROI_LastIdx = iCurrentIdx;
                break;
            }
        }
    }

    //------------------------------------------------------------------//
    //
    //	DataFitterTask::dofit()
    //
    //------------------------------------------------------------------//
    DataFitterTask::FitStatus DataFitterTask::dofit()
    {
        //DEBUG_STREAM << "in DataFitterTask::dofit()" << ENDLOG;
        std::vector<double> vdDataX_ROI;
        std::vector<double> vdDataY_ROI;
        std::vector<double> oFullExperimentalDataTmp;
        FitStatus ret_val;
        size_t nb_params = fit_function_->getNbParameters();
        DataP new_data(new Data());
        new_data->config = config_;
        new_data->fit_function_equation = fit_function_equation_;

        //-----------------
        // Read the source data
        //-----------------
        // Read X attribute
        Tango::DeviceAttribute data_x = source_x_->read();

        if (data_x.dim_x <= 4)
        {
            throw_devfailed("WRONG_ATTRIBUTE", "Not enough experimental data X to fit", "DataFitterTask::dofit");
        }
        if (data_x.dim_y > 0)
        {
            throw_devfailed("WRONG_ATTRIBUTE", "Cannot fit a 2D attribute", "DataFitterTask::dofit");
        }

		extract_data_as_double( data_x, source_x_info_, new_data->experimental_data_x );

        // Check ROI parameters before launching fit.
        // Find DataX_Min and DataX_Max indexes, to match with range defined by the user.
        double dMinRange = new_data->config.m_oROI_params.dExperimentalDataX_Min;
        double dMaxRange = new_data->config.m_oROI_params.dExperimentalDataX_Max;
        size_t oROI_FirstIdx = 0;
        size_t oROI_LastIdx = 0;

		try
		{
			checkROI_Coherency(new_data->experimental_data_x, dMinRange, dMaxRange, oROI_FirstIdx, oROI_LastIdx);
		}
		// If a non-DevFailed exception is thrown, it will be catch out of the function
        catch (Tango::DevFailed& ex)
        {
			ret_val.state = Tango::ALARM;
            ret_val.status = ex.errors[0].desc._ptr;
            this->reset_data();
			return ret_val;
        }

        //-----------------
        // Read Y attribute
        Tango::DeviceAttribute data_y = source_y_->read();

        if (data_y.dim_x <= 4)
        {
            throw_devfailed("WRONG_ATTRIBUTE", "Not enough experimental data Y to fit", "DataFitterTask::dofit");
        }
        if (data_y.dim_y > 0)
        {
            throw_devfailed("WRONG_ATTRIBUTE", "Cannot fit a 2D attribute", "DataFitterTask::dofit");
        }
        extract_data_as_double(data_y, source_y_info_, new_data->experimental_data_y);

        if (new_data->experimental_data_x.size() != new_data->experimental_data_y.size())
        {
            throw_devfailed("WRONG_ATTRIBUTE", "X and Y data do not have the same size", "DataFitterTask::dofit");
        }

        //-----------------
        // Read Sigma attribute
        if (config_.data_source.use_sigma)
        {
            Tango::DeviceAttribute data_sigma = source_sigma_->read();
			extract_data_as_double( data_sigma, source_sigma_info_, new_data->experimental_data_sigma );

			if ( new_data->experimental_data_sigma.size() != new_data->experimental_data_x.size() )
			{
				throw_devfailed( "WRONG_ATTRIBUTE", "X and SIGMA data do not have the same size", "DataFitterTask::dofit" );
			}
        }
        else
        {
            // If not defined, create a Data Sigma Vector with the default value "1"
			new_data->experimental_data_sigma.resize( new_data->experimental_data_x.size() );
			const double default_sigma = 1;
			std::fill( new_data->experimental_data_sigma.begin(), new_data->experimental_data_sigma.end(), default_sigma );
        }

        // extract NaN data (TANGODEVIC-964)
        std::size_t nb_data = new_data->experimental_data_x.size();
        //- TANGODEVIC-1953 TODO : ajouter numéro ticket et vérifier std::max!!
        for(std::size_t idx=0; idx < nb_data; idx++)
        {
            if ( std::isnan(new_data->experimental_data_x.at(idx)) || std::isnan(new_data->experimental_data_y.at(idx)) )
            {
                new_data->experimental_data_x.erase(new_data->experimental_data_x.begin()+idx);
                new_data->experimental_data_y.erase(new_data->experimental_data_y.begin()+idx);
                new_data->experimental_data_sigma.erase(new_data->experimental_data_sigma.begin()+idx);
            }
        }

        // do the fit and fill new Data instance
        // we need to use gsl_vector to interface with the DataFitterlib
        // they will NOT own their memory and will be used here only as wrapper to the real data
        // which are stored in the std::vector<>

        int status = GSL_FAILURE;
        // Prepare nb of data to use for fitting
        size_t oNbDataROI = oROI_LastIdx - oROI_FirstIdx + 1;

        gsl_vector gsldata_x;
        gsldata_x.size = oNbDataROI;
        gsldata_x.stride = 1;
		// Set the position of the data to fit at the first element of the ROI
        gsldata_x.data = &(new_data->experimental_data_x.at(oROI_FirstIdx));
        gsldata_x.block = NULL;
        gsldata_x.owner = 0;

        gsl_vector gsldata_y;
        gsldata_y.size = oNbDataROI;
        gsldata_y.stride = 1;
		// Set the position of the data to fit at the first element of the ROI
         gsldata_y.data = &(new_data->experimental_data_y.at(oROI_FirstIdx));
        gsldata_y.block = NULL;
        gsldata_y.owner = 0;

        gsl_vector gsldata_sigma;
        gsldata_sigma.size = oNbDataROI;
        gsldata_sigma.stride = 1;
		// Set the position of the data to fit at the first element of the ROI
        gsldata_sigma.data = &(new_data->experimental_data_sigma.at(oROI_FirstIdx));
        gsldata_sigma.block = NULL;
        gsldata_sigma.owner = 0;

        std::vector<double> params(nb_params);
        gsl_vector gsldata_params;
        gsldata_params.size = nb_params;
        gsldata_params.stride = 1;
        gsldata_params.data = &params.front();
        gsldata_params.block = NULL;
        gsldata_params.owner = 0;

        params[0] = config_.fit_params.first_guess.position;
        params[1] = config_.fit_params.first_guess.width;
        params[2] = config_.fit_params.first_guess.height;
        //debut TANGODEVIC-235
        if (nb_params == 4)
        {
            params[3] = config_.fit_params.first_guess.background;
        }
        //if background = a*x+b
        else if (nb_params > 4)
        {
            params[3] = config_.fit_params.first_guess.background_a;
            params[4] = config_.fit_params.first_guess.background_b;
        }
        //fin TANGODEVIC-235
        FittingData input_data = { oNbDataROI, &gsldata_x, &gsldata_y, &gsldata_sigma };

        FittingConfiguration input_config = {
            config_.fit_params.is_reversed,
            config_.fit_params.search_stopping_params + 1,
            config_.fit_params.scaled_jacobian,
            config_.fit_params.nb_iter_max,
            config_.fit_params.epsilon
        };

		// Do the fit
        status = fit_function_->doFit(input_data, input_config, &gsldata_params,
            config_.fit_params.auto_guess);

        switch (status)
        {
        case GSL_SUCCESS:
        {
            ret_val.state = Tango::RUNNING;
            ret_val.status = "Fit successful";
        }
        break;
        case GSL_FAILURE:
        {
            ret_val.state = Tango::FAULT;
            ret_val.status = "Error during fit";
        }
        break;
        case GSL_CONTINUE:
        {
            ret_val.state = Tango::ALARM;
            ret_val.status = "Fit does not converge";
        }
        break;
        case GSL_ETOLF:
        {
            ret_val.state = Tango::ALARM;
            ret_val.status = "Cannot reach specified tolerance in function";
        }
        break;
        case GSL_ETOLX:
        {
            ret_val.state = Tango::ALARM;
            ret_val.status = "Cannot reach specified tolerance in parameters";

        }
        break;
        case GSL_ETOLG:
        {
            ret_val.state = Tango::ALARM;
            ret_val.status = "Cannot reach specified tolerance in gradient";
        }
        break;
        }

        // Add condition to avoid data reset when fit does not converge
        if ((ret_val.state != Tango::RUNNING) && (status != GSL_CONTINUE))
        {
            this->reset_data();
            return ret_val;
        }

		// Update initial values if necessary
        if (config_.fit_params.auto_guess)
        {
            new_data->first_guess.position = fit_function_->getInitialGuess(0);
            new_data->first_guess.width = fit_function_->getInitialGuess(1);
            new_data->first_guess.height = fit_function_->getInitialGuess(2);
            //debut TANGODEVIC-235

            if (nb_params == 4)
            {
                new_data->first_guess.background = fit_function_->getInitialGuess(3);
            }
            else if (nb_params > 4)
            {
                new_data->first_guess.background_a = fit_function_->getInitialGuess(3);
                new_data->first_guess.background_b = fit_function_->getInitialGuess(4);
            }
            //fin TANGODEVIC-235
        }
        else
        {
            new_data->first_guess = config_.fit_params.first_guess;
        }

        new_data->nb_parameters = nb_params;
        new_data->result.position = fit_function_->getParameter(0);
        new_data->result.width = fit_function_->getParameter(1);
        new_data->result.height = fit_function_->getParameter(2);
        //debut TANGODEVIC-235

        if (nb_params == 4)
            new_data->result.background = fit_function_->getParameter(3);
        if (nb_params > 4)
        {
            new_data->result.background_a = fit_function_->getParameter(3);
            new_data->result.background_b = fit_function_->getParameter(4);

        }
        //fin TANGODEVIC-235

        new_data->fit_params.resize(nb_params);
        for (size_t i = 0; i < nb_params; i++)
        {
            new_data->fit_params[i] = fit_function_->getParameter(i);
        }

        new_data->fwhm = fit_function_->getFWHM();
        new_data->hwhm = fit_function_->getHWHM();

        if (config_.fit_params.fitting_function_type.str == "sigmoid" ||
            config_.fit_params.fitting_function_type.str == "sigmoidb")
        {
            new_data->x_low = fit_function_->getXLow();
            new_data->x_high = fit_function_->getXHigh();
        }
        new_data->nb_iteration = fit_function_->getNbIterations();

		//-------------------------
        // Generate fitted data
		//-------------------------
        size_t fitted_data_size = config_.fitted_data.same_size_as_data ?
            oNbDataROI :
            config_.fitted_data.nb_points;

        new_data->nb_data = fitted_data_size;

        new_data->fitted_data_x.resize(fitted_data_size);
        new_data->fitted_data_y.resize(fitted_data_size);
        new_data->derivative_fitted_data_y.resize(fitted_data_size);

        gsl_vector gsldata_fittedx;
        gsldata_fittedx.size = fitted_data_size;
        gsldata_fittedx.stride = 1;
        gsldata_fittedx.data = &new_data->fitted_data_x.front();
        gsldata_fittedx.block = NULL;
        gsldata_fittedx.owner = 0;

        gsl_vector gsldata_fittedy;
        gsldata_fittedy.size = fitted_data_size;
        gsldata_fittedy.stride = 1;
        gsldata_fittedy.data = &new_data->fitted_data_y.front();
        gsldata_fittedy.block = NULL;
        gsldata_fittedy.owner = 0;

        gsl_vector gsldata_derivfittedy;
        gsldata_derivfittedy.size = fitted_data_size;
        gsldata_derivfittedy.stride = 1;
        gsldata_derivfittedy.data = &new_data->derivative_fitted_data_y.front();
        gsldata_derivfittedy.block = NULL;
        gsldata_derivfittedy.owner = 0;


        if (config_.fitted_data.same_size_as_data)
        {
            fit_function_->generateFunctionFit(fitted_data_size,
                &gsldata_x,
                &gsldata_fittedx,
                &gsldata_fittedy);

            fit_function_->generateDerivativeFunctionFit(fitted_data_size,
                &gsldata_x,
                &gsldata_fittedx,
                &gsldata_derivfittedy);
        }
        else
        {
            fit_function_->generateFunctionFit(config_.fitted_data.start_x,
                config_.fitted_data.resolution_x,
                config_.fitted_data.nb_points,
                &gsldata_fittedx,
                &gsldata_fittedy);

            fit_function_->generateDerivativeFunctionFit(config_.fitted_data.start_x,
                config_.fitted_data.resolution_x,
                config_.fitted_data.nb_points,
                &gsldata_fittedx,
                &gsldata_derivfittedy);
        }

        new_data->quality_factor = 100 * fit_function_->computeDeterminationCoefficient(&gsldata_y, &gsldata_fittedy);
        new_data->fstatistics = fit_function_->computeObservedFStatistic(&gsldata_y, &gsldata_fittedy);


        // min/max/centroid
        new_data->minimum = new_data->experimental_data_y[0];
        //debut TANGODEVIC-1493
        new_data->minimum_pos = new_data->experimental_data_x[0];
        //fin TANGODEVIC-1493
        new_data->maximum = new_data->experimental_data_y[0];
        //debut TANGODEVIC-1493
        new_data->maximum_pos = new_data->experimental_data_x[0];
        //fin TANGODEVIC-1493
        new_data->derivative_minimum = new_data->derivative_fitted_data_y[0];
        //debut TANGODEVIC-1493
        new_data->derivative_minimum_pos = new_data->fitted_data_x[0];
        //fin TANGODEVIC-1493
        new_data->derivative_maximum = new_data->derivative_fitted_data_y[0];
        //debut TANGODEVIC-1493
        new_data->derivative_maximum_pos = new_data->fitted_data_x[0];
        //fin TANGODEVIC-1493

        new_data->centroid = new_data->experimental_data_x[0] * new_data->experimental_data_y[0];
        double sum_y = new_data->experimental_data_y[0];

        for (int i = 1; i < new_data->nb_data; i++)
        {
            if (new_data->experimental_data_y[i] < new_data->minimum)
            {
                new_data->minimum = new_data->experimental_data_y[i];
                new_data->minimum_pos = new_data->experimental_data_x[i];
            }

            if (new_data->experimental_data_y[i] > new_data->maximum)
            {
                new_data->maximum = new_data->experimental_data_y[i];
                new_data->maximum_pos = new_data->experimental_data_x[i];
            }

            if (new_data->derivative_fitted_data_y[i] < new_data->derivative_minimum)
            {
                new_data->derivative_minimum = new_data->derivative_fitted_data_y[i];
                new_data->derivative_minimum_pos = new_data->fitted_data_x[i];
            }

            if (new_data->derivative_fitted_data_y[i] > new_data->derivative_maximum)
            {
                new_data->derivative_maximum = new_data->derivative_fitted_data_y[i];
                new_data->derivative_maximum_pos = new_data->fitted_data_x[i];
            }

            new_data->centroid += new_data->experimental_data_x[i] * new_data->experimental_data_y[i];
            sum_y += new_data->experimental_data_y[i];
        }

        new_data->centroid /= sum_y;

        // publish new data
        {
            yat::MutexLock guard(this->mutex_);
            data_ = new_data;
        }

        return ret_val;
    }

    //------------------------------------------------------------------//
    //
    //	DataFitterTask::set_state_status ()
    //
    //------------------------------------------------------------------//

    void DataFitterTask::set_state_status(Tango::DevState state, std::string status)
    {
        yat::MutexLock guard(this->mutex_);
        state_ = state;
        status_ = status;
    }
}
