#ifndef _RFFBTASK_H_
#define _RFFBTASK_H_

#include <boost/smart_ptr.hpp>
#include <yat/threading/Task.h>

#include <gsl/gsl_vector.h> 
#include <gsl/gsl_multifit_nlin.h> 

#include <FittingFunctionFactory.h>

#include <tango.h>

namespace DataFitter_ns
{
//debut TANGODEVIC-1288

//default value of epsilon
const double min_epsilon = 0.0001;

//fin TANGODEVIC-1288

// for tango string attribute, there is a very annoying constraint :
// attr.set_value( ) must be called with an l-value
// so it is impossible to do attr.set_value( some_string.c_str() )
struct tango_string
{
    tango_string( const char* p = 0 )
    {
        if (p)
            str = p;
        else
            str = "";
        ptr = const_cast<char*>(str.c_str());
    }

    tango_string( const tango_string& other )
    {
        str = other.str;
        ptr = const_cast<char*>(str.c_str());
    }

    tango_string& operator = ( const char* p )
    {
        if (p)
            str = p;
        else
            str = "";
        ptr = const_cast<char*>(str.c_str());
        return *this;
    }

    tango_string& operator = ( const tango_string& other )
    {
        str = other.str;
        ptr = const_cast<char*>(str.c_str());
        return *this;

    }
    std::string str;
    char* ptr;
};

// initial parameters of the fit
// Note: For the parameters of function KnifeEdge(x):
//		- "mu" corresponds to "position"
//		- "sigma" corresponds to "width"
struct Params
{
    Params()
    : position( 0 ),
      width( 0 ),
      height( 0 ),
      background( 0 ),
//debut TANGODEVIC-235
//if background = a*x+b
      background_a(0),
      background_b(0)
//fin TANGODEVIC-235
    {}

    double position;
    double width;
    double height;
    double background;
 //debut TANGODEVIC-235
    double background_a;
    double background_b;
  //fin TANGODEVIC-235
};


struct DataFitterConfig
{
    // data source
    struct DataSource
    {
        DataSource()
        : use_sigma(false)
        {}

        tango_string attr_name_x;
        tango_string attr_name_y;
        tango_string attr_name_s;
        bool use_sigma;
    };

    // type of stopping method
    enum SearchStoppingParams
    {
        DELTA,
        GRADIENT
    };

    // fit algo parameters
    struct FitParams
    {
        FitParams()
        : fitting_function_type( "gaussianb" ),
          nb_iter_max( 100 ),
          epsilon( min_epsilon ),
          auto_guess( true ),
          scaled_jacobian( true ),
          is_reversed( false),
          search_stopping_params( DELTA )
        {}

        tango_string fitting_function_type;
        long nb_iter_max;
        double epsilon;
        bool auto_guess;
        bool scaled_jacobian;
        bool is_reversed;
        SearchStoppingParams search_stopping_params;
        Params first_guess;
    };

    // params for generating the fitted data
    struct GenerationParams
    {
        GenerationParams()
        : same_size_as_data(true),
          start_x( 0 ),
          resolution_x( 0 ),
          nb_points( 100 )
        {
        }

        bool same_size_as_data; // if true, don't use the other params

        double start_x;
        double resolution_x;
        long   nb_points;
    };

    // params for defining ROI of experimental data
    struct ROI_Params
    {
        ROI_Params()
            : dExperimentalDataX_Min(0.0),
            dExperimentalDataX_Max(0.0)
        {
        }
        double dExperimentalDataX_Min;
        double dExperimentalDataX_Max;
    };

    DataFitterConfig()
    : manual_mode( true )
    {}

    bool manual_mode; // if true, fit is triggered only by StartFit command
    DataSource data_source;
    FitParams fit_params;
    GenerationParams fitted_data;
    ROI_Params m_oROI_params;
};

struct Data
{
    Data()
    : nb_parameters(0),
      nb_data(0),
      hwhm(0),
      fwhm(0),
      x_low(0),
      x_high(0),
      centroid(0),
      minimum(0),
      minimum_pos(0),
      maximum(0),
      maximum_pos(0),
      derivative_minimum(0),
      derivative_minimum_pos(0),
      derivative_maximum(0),
      derivative_maximum_pos(0),
      nb_iteration(0),
      quality_factor(0),
      fstatistics(0)
    {}

    DataFitterConfig config;
    Params first_guess; // in auto guess mode it gives the auto-found initial guess

    long   nb_parameters;
    long   nb_data;

    Params result;

    double hwhm;
    double fwhm;
    double x_low;
    double x_high;

    double centroid;

    double minimum;
    double minimum_pos;
    double maximum;
    double maximum_pos;

    double derivative_minimum;
    double derivative_minimum_pos;
    double derivative_maximum;
    double derivative_maximum_pos;

    long nb_iteration;
    double quality_factor;
    double fstatistics;

    std::vector<double> experimental_data_x;     // Extracted data from full input experimental data X (based on ROI)
    std::vector<double> experimental_data_y;     // Extracted data from full input experimental data Y (based on ROI)
    std::vector<double> experimental_data_sigma; // Extracted data from full input experimental data Sigma (based on ROI)
    std::vector<double> fitted_data_x;
    std::vector<double> fitted_data_y;

    std::vector<double> derivative_fitted_data_y;

    std::vector<double> fit_params;

    tango_string fit_function_equation;
};

typedef boost::shared_ptr<Data> DataP;



//
class DataFitterTask : public yat::Task,
public Tango::LogAdapter
{
public:
    DataFitterTask(Tango::DeviceImpl* dev);

    ~DataFitterTask();

    DataP get_data( void )
    {
        yat::MutexLock guard(mutex_);
        return data_;
    }

    Tango::DevState get_state( void ) {
        yat::MutexLock guard(mutex_);
        return state_;
    }

    std::string get_status( void )
    {
        yat::MutexLock guard(mutex_);
        return status_;
    }

    void configure_fit( DataFitterConfig::FitParams& params );

    void configure_source( DataFitterConfig::DataSource& data_source );

    void configure_generation(DataFitterConfig::GenerationParams& fitted_data);

    void configureROI(DataFitterConfig::ROI_Params& oROI_Params);

    void set_manual_mode(bool manual);

    void start(  );

    void stop( );

protected:
    virtual void handle_message( yat::Message& msg );

private:

    void configure_fit_i();
    void configure_source_i();

//debut TANGODEVIC-1263
    void reset_data();
//fin TANGODEVIC-1263

    void checkROI_Coherency(std::vector<double> &oData
        , double dMinRange
        , double dMaxRange
        , size_t& oROI_FirstIdx
        , size_t& oROI_LastIdx);

    struct FitStatus
    {
        Tango::DevState state;
        std::string     status;
    };
    FitStatus dofit();

    boost::scoped_ptr<FittingFunction> fit_function_;
    tango_string fit_function_equation_;
    bool fit_ready_;

    DataFitterConfig config_;
    bool manual_mode_;
    boost::scoped_ptr<Tango::AttributeProxy> source_x_;
    Tango::AttributeInfoEx            source_x_info_;

    boost::scoped_ptr<Tango::AttributeProxy> source_y_;
    Tango::AttributeInfoEx            source_y_info_;

    boost::scoped_ptr<Tango::AttributeProxy> source_sigma_;
    Tango::AttributeInfoEx            source_sigma_info_;

    bool source_ready_;

    yat::Mutex      mutex_;
    Tango::DevState state_;
    std::string     status_;

    void set_state_status( Tango::DevState state, std::string status );

    DataP           data_;
};

struct DataFitterTaskExiter
{
    void operator() ( DataFitterTask* t )
    {
        try
        {
            t->exit();
        }
        catch(...)
        {
            // ignore error
        }
    }
};

typedef boost::shared_ptr< DataFitterTask > DataFitterTaskP;

}

#endif // _RFFBTASK_H_
