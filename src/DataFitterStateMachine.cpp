static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Calculation/DataFitter/src/DataFitterStateMachine.cpp,v 1.25 2009-02-26 15:46:42 julien_malik Exp $";
//+=============================================================================
//
// file :         DataFitterStateMachine.cpp
//
// description :  C++ source for the DataFitter and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: julien_malik $
//
// $Revision: 1.25 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

// #include <tango.h>
// #include <DataFitter.h>
#include "DataFitterClass.h"

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace DataFitter_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_deviceAttributeNameX_allowed
// 
// description : 	Read/Write allowed for deviceAttributeNameX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_deviceAttributeNameX_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_deviceAttributeNameY_allowed
// 
// description : 	Read/Write allowed for deviceAttributeNameY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_deviceAttributeNameY_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_deviceAttributeNameSigma_allowed
// 
// description : 	Read/Write allowed for deviceAttributeNameSigma attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_deviceAttributeNameSigma_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_useSigma_allowed
// 
// description : 	Read/Write allowed for useSigma attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_useSigma_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_nbData_allowed
// 
// description : 	Read/Write allowed for nbData attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_nbData_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittingFunctionType_allowed
// 
// description : 	Read/Write allowed for fittingFunctionType attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fittingFunctionType_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}

//debut TANGODEVIC-1318

//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittingFunctionType_allowed
// 
// description : 	Read/Write allowed for fittingFunctionType attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_knownFittersList_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}

//fin TANGODEVIC-1318

//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_functionEquation_allowed
// 
// description : 	Read/Write allowed for functionEquation attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_functionEquation_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_nbIterationMax_allowed
// 
// description : 	Read/Write allowed for nbIterationMax attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_nbIterationMax_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_epsilon_allowed
// 
// description : 	Read/Write allowed for epsilon attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_epsilon_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_startingX_allowed
// 
// description : 	Read/Write allowed for startingX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_startingX_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_resolutionX_allowed
// 
// description : 	Read/Write allowed for resolutionX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_resolutionX_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_nbPointsToGenerate_allowed
// 
// description : 	Read/Write allowed for nbPointsToGenerate attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_nbPointsToGenerate_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fitMode_allowed
// 
// description : 	Read/Write allowed for fitMode attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fitMode_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_nbParameters_allowed
// 
// description : 	Read/Write allowed for nbParameters attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_nbParameters_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialsParametersMode_allowed
// 
// description : 	Read/Write allowed for initialsParametersMode attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialsParametersMode_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialPosition_allowed
// 
// description : 	Read/Write allowed for initialPosition attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialPosition_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialWidth_allowed
// 
// description : 	Read/Write allowed for initialWidth attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialWidth_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialHeight_allowed
// 
// description : 	Read/Write allowed for initialHeight attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialHeight_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialBackground_allowed
// 
// description : 	Read/Write allowed for initialBackground attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialBackground_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_position_allowed
// 
// description : 	Read/Write allowed for position attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_position_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_width_allowed
// 
// description : 	Read/Write allowed for width attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_width_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_height_allowed
// 
// description : 	Read/Write allowed for height attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_height_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_background_allowed
// 
// description : 	Read/Write allowed for background attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_background_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittedDataSameSizeAsData_allowed
// 
// description : 	Read/Write allowed for fittedDataSameSizeAsData attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fittedDataSameSizeAsData_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_hwhm_allowed
// 
// description : 	Read/Write allowed for hwhm attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_hwhm_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fwhm_allowed
// 
// description : 	Read/Write allowed for fwhm attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fwhm_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_nbIterations_allowed
// 
// description : 	Read/Write allowed for nbIterations attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_nbIterations_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_searchStoppingMethod_allowed
// 
// description : 	Read/Write allowed for searchStoppingMethod attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_searchStoppingMethod_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_useScaled_allowed
// 
// description : 	Read/Write allowed for useScaled attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_useScaled_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_xLow_allowed
// 
// description : 	Read/Write allowed for xLow attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_xLow_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_xHigh_allowed
// 
// description : 	Read/Write allowed for xHigh attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_xHigh_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_determinationQualityFactor_allowed
// 
// description : 	Read/Write allowed for determinationQualityFactor attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_determinationQualityFactor_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fStatisticQualityFactor_allowed
// 
// description : 	Read/Write allowed for fStatisticQualityFactor attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fStatisticQualityFactor_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_experimentalDataX_allowed
// 
// description : 	Read/Write allowed for experimentalDataX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_experimentalDataX_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_experimentalDataY_allowed
// 
// description : 	Read/Write allowed for experimentalDataY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_experimentalDataY_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_experimentalDataSigma_allowed
// 
// description : 	Read/Write allowed for experimentalDataSigma attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_experimentalDataSigma_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittedFunctionParameters_allowed
// 
// description : 	Read/Write allowed for fittedFunctionParameters attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fittedFunctionParameters_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittedDataX_allowed
// 
// description : 	Read/Write allowed for fittedDataX attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fittedDataX_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_fittedDataY_allowed
// 
// description : 	Read/Write allowed for fittedDataY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_fittedDataY_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_centroid_allowed
// 
// description : 	Read/Write allowed for centroid attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_centroid_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_minimum_allowed
// 
// description : 	Read/Write allowed for minimum attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_minimum_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_minimumPos_allowed
// 
// description : 	Read/Write allowed for minimumPos attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_minimumPos_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_maximum_allowed
// 
// description : 	Read/Write allowed for maximum attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_maximum_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_maximumPos_allowed
// 
// description : 	Read/Write allowed for maximumPos attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_maximumPos_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_minimumDeriv_allowed
// 
// description : 	Read/Write allowed for minimumDeriv attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_minimumDeriv_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_minimumDerivPos_allowed
// 
// description : 	Read/Write allowed for minimumDerivPos attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_minimumDerivPos_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_maximumDeriv_allowed
// 
// description : 	Read/Write allowed for maximumDeriv attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_maximumDeriv_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_maximumDerivPos_allowed
// 
// description : 	Read/Write allowed for maximumDerivPos attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_maximumDerivPos_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_derivedFittedDataY_allowed
// 
// description : 	Read/Write allowed for derivedFittedDataY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_derivedFittedDataY_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_reverseY_allowed
// 
// description : 	Read/Write allowed for reverseY attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_reverseY_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}

// debut TANGODEVIC-235 
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialBackgroundA_allowed
// 
// description : 	Read/Write allowed for initialBackgroundA attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialBackgroundA_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_initialBackgroundB_allowed
// 
// description : 	Read/Write allowed for initialBackgroundB attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_initialBackgroundB_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_backgroundA_allowed
// 
// description : 	Read/Write allowed for backgroundA attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_backgroundA_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_backgroundB_allowed
// 
// description : 	Read/Write allowed for backgroundB attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_backgroundB_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
// fin TANGODEVIC-235 
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_experimentalDataX_Min_allowed
// 
// description : 	Read/Write allowed for experimentalDataX_Min attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_experimentalDataX_Min_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_experimentalDataX_Max_allowed
// 
// description : 	Read/Write allowed for experimentalDataX_Max attribute.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_experimentalDataX_Max_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_StartFit_allowed
// 
// description : 	Execution allowed for StartFit command.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_StartFit_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_WriteExperimentalData_allowed
// 
// description : 	Execution allowed for WriteExperimentalData command.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_WriteExperimentalData_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		DataFitter::is_WriteFittedData_allowed
// 
// description : 	Execution allowed for WriteFittedData command.
//
//-----------------------------------------------------------------------------
bool DataFitter::is_WriteFittedData_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

    //	Re-Start of Generated Code
	return true;
}

}	// namespace DataFitter_ns
