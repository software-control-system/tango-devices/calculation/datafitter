cmake_minimum_required(VERSION 2.6)
set( CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS true)

project( ds_DataFitter )
set( CMAKE_DEBUG_POSTFIX "-d" )
set( CMAKE_FIND_ROOT_PATH ${CMAKE_INSTALL_PREFIX}/.cmake )

if (WIN32)
  remove_definitions( -D_WINDOWS )
  add_definitions( -D_CONSOLE -DWIN32_LEAN_AND_MEAN -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -DLOG4TANGO_HAVE_INT64_T )
endif()

find_package( Tango REQUIRED )
find_package( YAT REQUIRED )

add_definitions( ${TANGO_DEFINITIONS} )


set ( SW_SUPPORT $ENV{SOLEIL_ROOT}/sw-support )

include_directories( . 
                     ${TANGO_INCLUDE_DIRS}
                     ${SW_SUPPORT}/dev/include
                     ${SW_SUPPORT}/Utils/include
                     ${SW_SUPPORT}/DataFitterLib/include
                     ${SW_SUPPORT}/Interpolator/include
                     ${SW_SUPPORT}/Exceptions/include
                     ${SW_SUPPORT}/GSL/include
                     ${YAT_INCLUDE_DIRS}
                     ${SW_SUPPORT}/Boost/include )

set( DEVSRCS main.cpp ClassFactory.cpp DataFitter.cpp DataFitterClass.cpp DataFitterStateMachine.cpp  DataFitterTask.cpp DataFitter.h DataFitterClass.h DataFitterTask.h  )

source_group( "src" FILES ${DEVSRCS} )

add_executable( ds_DataFitter ${DEVSRCS} )

target_link_libraries( ds_DataFitter
                       yat-static 
                       tango
                       ${SW_SUPPORT}/Utils/lib/libUtilsd.lib 
                       ${SW_SUPPORT}/DataFitterLib/lib/libDataFitterLibd.lib 
                       ${SW_SUPPORT}/Interpolator/lib/libInterpolatord.lib 
                       ${SW_SUPPORT}/Utils/lib/libUtilsd.lib 
                       ${SW_SUPPORT}/Exceptions/lib/libExceptionsd.lib 
                       ${SW_SUPPORT}/GSL/lib/libGSLd.lib 
                       ${SW_SUPPORT}/GSL/lib/libGSLcblasd.lib )
